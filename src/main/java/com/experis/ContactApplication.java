package com.experis;

import com.experis.people.Person;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class ContactApplication {


    public static final String ANSI_RESET = "\u001B[0m"; //Needed to make the text red
    public static final String ANSI_RED = "\u001B[31m"; //Needed to make the text red

    public static ArrayList getPersonArray(){//creating an array wich will be search



        ArrayList<Person> personArrayList = new ArrayList<>();


        personArrayList.add(new Person("Sven Dahlström", "Anna Koskulls gata 3E", "0761047246", "6648", "20 januari 1988"));
        personArrayList.add(new Person("Martin Sandström", "Gösta Edströmsgata 5B", "123456789", "5540", "16 november 1995"));
        personArrayList.add(new Person("Joakim den blodige", "Köttgränden 82d", "987654321", "5462", "15 augusti 1991"));
        personArrayList.add(new Person("Calle lundgren", "Mörnensväg 8", "123454321", "46532", "20 maj 1990"));
        personArrayList.add(new Person("Krister Pettersson", "George Lyckes väg 6", "234565432", "2563", "23 april 1947"));
        personArrayList.add(new Person("Mats Alm", "Solna vägen 25", "34556765", "1245", "6 maj 1975"));
        personArrayList.add(new Person("Oskar Ohlsson", "Norra köpmangatan 22", "09876543", "7895", "19 februari 1986"));
        personArrayList.add(new Person("Farmen Kristns", "Västergatan 2", "987654321", "4567", "2 oktober 1955"));
        personArrayList.add(new Person("Sebastian Dawkins", "Rökarebacke 1a", "3456789", "1547", "25 maj 1968"));
        personArrayList.add(new Person("Biff Bertil", "Augustenborgsgatan 7", "47536786", "1234", " 9 januari 2002"));

        return personArrayList;
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in); //in scanner
        int noresults=0;    //a counter who will know if you got a result or not
        String highLightedAwnser;
       
        ArrayList<Person> personArrayList = new ArrayList<>(); //creating an empty list

        personArrayList=getPersonArray();//getting the persons to search

        System.out.println("Welcome to the person searcher. Type a search term for results or EXIT to quit");

        String searchTerm;

        do{//looping until its the user types exit
            System.out.println("Enter SearchTerm: ");
            searchTerm=scan.nextLine();//scanning the input from the console
            if(!searchTerm.isEmpty()){ //a usecase for a non-empty string as a searchterm

                for(Person i: personArrayList){
                    if (i.toString().contains(searchTerm)){ //looking at the string and se if the searchterm is in it
                        System.out.println(highLightedAwnser=i.toString().replace(searchTerm,ANSI_RED+searchTerm+ANSI_RESET)); //replacing the serchterm with red text

                    }else {
                        noresults++;
                    }
                }
            }else{
                System.out.println("You must put something in"); //if inputstring is empty
            }

            if(!searchTerm.equals("EXIT") && noresults==personArrayList.size()){
                System.out.println("We didnt find anything"); //if the searchterm is resultless
            }
            noresults=0;
        } while (!searchTerm.equals("EXIT"));

    }
}
