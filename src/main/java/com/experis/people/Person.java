package com.experis.people;

public class Person { //Person object
    String name;
    String adress;
    String mobilenumber;
    String worknumber;
    String dateofbirth;

    public Person() { //empty constructor
    }

    public Person(String name, String adress, String mobilenumber, String worknumber, String dateofbirth) { //Constructor
        this.name = name;
        this.adress = adress;
        this.mobilenumber = mobilenumber;
        this.worknumber = worknumber;
        this.dateofbirth = dateofbirth;
    }


    @Override
    public String toString() { //Tostring method wich will aid in the wordsearch of the objects
        return "Person{" +
                "name:'" + name + '\'' +
                ", Adress:'" + adress + '\'' +
                ", mobilenumber:" + mobilenumber +
                ", worknumber:" + worknumber +
                ", dateofbirth:'" + dateofbirth + '\'' +
                '}';
    }
}
